package com.celestialapps.roomchats.activity.preferences;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;

import com.celestialapps.roomchats.activity.LanguageActivity;
import com.celestialapps.roomchats.activity.ThemeActivity;

import java.util.Locale;

/**
 * Created by Sergey on 07.05.2017.
 */

public class SettingsPreferencesActivity extends LanguageActivity {
    private static final String TAG = "SettingsPreferencesAct";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) createSettingsPreferencesFragment();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onStop()");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop()");
        super.onStop();
    }


    private void createSettingsPreferencesFragment() {
        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content, new SettingsPreferencesFragment())
                .commit();
    }

}

