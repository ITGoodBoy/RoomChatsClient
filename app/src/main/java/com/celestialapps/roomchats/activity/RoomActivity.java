package com.celestialapps.roomchats.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.Toast;

import com.celestialapps.roomchats.activity.adapters.RoomAdapter;
import com.celestialapps.roomchats.chat.ChatClient;
import com.celestialapps.roomchats.model.User;
import com.celestialapps.roomchats.networking.RoomChatsService;
import com.celestialapps.roomchats.networking.interfaces.RoomApi;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class RoomActivity extends ToolbarActivity {
    private final String TAG = "RoomActivity";


    private List<String> messages = new LinkedList<>();
    private RoomApi roomApi = RoomChatsService.getRoomApi();

    private ChatClient chatClient;
    private User user;
    private RoomAdapter roomAdapter;

    @BindView(R.id.room_message_list) RecyclerView roomMessageList;
    @BindView(R.id.material_send_message) MaterialEditText materialSendMessage;
    @BindView(R.id.btnSend) Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        ButterKnife.bind(this);
        initializeViews();
        user = (User) getIntent().getSerializableExtra("user");
        refreshMessages();
    }

    private void initializeViews() {
        Log.d(TAG, "initializeViews()");
        roomAdapter = new RoomAdapter(this, messages);
        roomMessageList.setLayoutManager(new LinearLayoutManager(this));
        roomMessageList.setAdapter(roomAdapter);

        materialSendMessage.setError("");
        materialSendMessage.setTextColor(getThemeTextColor());
        materialSendMessage.addTextChangedListener(materialTextWatcher());

        button.setOnClickListener(v -> {
            if (chatClient != null) {
                chatClient.sendMessage(materialSendMessage.getText().toString());
                materialSendMessage.setText("");
            }
        });
    }



    private TextWatcher materialTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                materialSendMessage.setError("");
            }
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
        if (chatClient != null) {
            chatClient.stop();
            chatClient = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume()");
    }


    private void refreshMessages() {
        Log.d(TAG, "refreshMessages()");
        final ProgressDialog progressDialog = createProgressDialog();
        progressDialog.show();

        roomApi.getRoomByName(user.getCurrentRoom().getName())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(roomResponse -> {
                    Log.d(TAG, roomResponse.getStatus().toString());
                    progressDialog.dismiss();
                    switch (roomResponse.getStatus()) {
                        case ROOM_OK:
                            messages.clear();
                            for (int i = 0; i < 50; i++) {
                                messages.add("");
                            }
                            messages.addAll(roomResponse.getRoom().getMessageList());
                            roomAdapter.notifyDataSetChanged();
                            roomMessageList.scrollToPosition(messages.size() - 1);
                            if (chatClient == null) {
                                chatClient = new ChatClient(user, roomMessageList);
                                chatClient.run();
                            }
                            break;
                        default:showToastMakeText("Неизвестная ошибка");
                    }
                }, throwable -> {
                    Log.d(TAG, throwable.getMessage(), throwable);
                    progressDialog.dismiss();
                    showToastMakeText("Ошибка подключения");
                });
    }



    public void showToastMakeText(String message) {
        Log.d(TAG, "showToastMakeText()");
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
    }


    @NonNull
    private ProgressDialog createProgressDialog() {
        Log.d(TAG, "createProgressDialog()");
        final ProgressDialog progressDialog = getThemeProgressDialog();
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("ждите...");
        return progressDialog;
    }


}
