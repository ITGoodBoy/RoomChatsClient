package com.celestialapps.roomchats.activity.preferences;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.util.Log;

import com.celestialapps.roomchats.activity.LoginActivity;
import com.celestialapps.roomchats.activity.R;

/**
 * Created by Sergey on 07.05.2017.
 */

public class SettingsPreferencesFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private final String TAG = "SettingsPreferencesFrag";

    public static final int RESTART_RESULT = 1;

    private Context context;
    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
        sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
        addPreferencesFromResource(R.xml.settings);
        registerSharedPreferenceChangeListener();
    }

    private void registerSharedPreferenceChangeListener() {
        Log.d(TAG, "registerSharedPreferenceChangeListener()");
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause()");
        super.onPause();
        unregisterSharedPreferenceChangeListener();
        setPreferenceScreen(null);
    }

    private void unregisterSharedPreferenceChangeListener() {
        Log.d(TAG, "unregisterSharedPreferenceChangeListener()");
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "onSharedPreferenceChanged()");
        if (key.equals("language")) {
            languagePreferenceChanged(key);
        }

        if (key.equals("theme")) {
            themePreferenceChanged(key);
        }
    }

    private void languagePreferenceChanged(String key) {
        ListPreference listPreference = (ListPreference) findPreference(key);
        if (listPreference == null) return;
        int index = listPreference.findIndexOfValue(listPreference.getValue());
        switch (index) {
            case 0:
                saveOrUpdateLanguagePreferences("ru");
                break;
            case 1:
                saveOrUpdateLanguagePreferences("en");
                break;
        }
    }

    private void themePreferenceChanged(String key) {
        if (key.equals("theme")) {
            ListPreference listPreference = (ListPreference) findPreference(key);
            if (listPreference == null) return;
            int index = listPreference.findIndexOfValue(listPreference.getValue());
            switch (index) {
                case 0:
                    saveOrUpdateThemePreferences(R.style.AppTheme_Red,
                            R.style.AppTheme_Red_ProgressDialog,
                            R.style.AppTheme_Red_AlertDialog);
                    break;
                case 1:
                    saveOrUpdateThemePreferences(R.style.AppTheme_Purple,
                            R.style.AppTheme_Purple_ProgressDialog,
                            R.style.AppTheme_Purple_AlertDialog);
                    break;
                case 2:
                    saveOrUpdateThemePreferences(R.style.AppTheme_Green,
                            R.style.AppTheme_Green_ProgressDialog,
                            R.style.AppTheme_Green_AlertDialog);
                    break;
                case 3:
                    saveOrUpdateThemePreferences(R.style.AppTheme_Blue,
                            R.style.AppTheme_Blue_ProgressDialog,
                            R.style.AppTheme_Blue_AlertDialog);
                    break;
                case 4:
                    saveOrUpdateThemePreferences(R.style.AppTheme_Pink,
                            R.style.AppTheme_Pink_ProgressDialog,
                            R.style.AppTheme_Pink_AlertDialog);
                    break;
            }
        }
    }


    private void saveOrUpdateLanguagePreferences(String language) {
        Log.d(TAG, "saveOrUpdateLanguagePreferences()");
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("language", language);
        boolean isSuccess = editor.commit();
        if (isSuccess) restartApp();
    }

    private void saveOrUpdateThemePreferences(int theme, int progressDialogTheme, int alertDialogTheme) {
        Log.d(TAG, "saveOrUpdateLanguagePreferences()");
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putInt("app_theme", theme);
        editor.putInt("progress_dialog_theme", progressDialogTheme);
        editor.putInt("alert_dialog_theme", alertDialogTheme);
        boolean isSuccess = editor.commit();
        if (isSuccess) restartApp();
    }


    private void restartApp() {
        getActivity().setResult(RESTART_RESULT);
        getActivity().finish();
    }

}
