package com.celestialapps.roomchats.activity.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.celestialapps.roomchats.activity.R;
import com.celestialapps.roomchats.activity.RoomsActivity;
import com.celestialapps.roomchats.model.User;
import com.celestialapps.roomchats.networking.RoomChatsService;
import com.celestialapps.roomchats.networking.interfaces.RoomApi;
import com.celestialapps.roomchats.networking.request.RoomRequest;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.LinkedList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sergey on 25.04.2017.
 */

public class RoomsCreateDialogFragment extends DialogFragment {
    private static final String TAG = "RoomsCreateDialogFragme";

    private final int ROOM_NAME_MIN_LENGTH = 3;
    private final int ROOM_NAME_MAX_LENGTH = 12;
    private final String ROOM_NAME_NOT_MIXED_ALPHABET_PATTERN = "^([a-zA-Z0-9-_\\\\.]*)|([а-яА-Я0-9-_\\\\.]*)$";

    private final int ROOM_PASSWORD_MIN_LENGTH = 4;
    private final int ROOM_PASSWORD_MAX_LENGTH = 10;

    private AlertDialog mAlertDialog;
    private View mView;
    private EditText mRoomName;
    private EditText mRoomPassword;
    private CheckBox mIsEmptyPassword;
    private DiscreteSeekBar mDiscreteSeekBar;

    private User mUser;
    private RoomApi mRoomApi = RoomChatsService.getRoomApi();
    private RoomsActivity mRoomsActivity;


    public static RoomsCreateDialogFragment newInstance(User user) {
        Log.d(TAG, "newInstance()");
        RoomsCreateDialogFragment roomsCreateDialogFragment = new RoomsCreateDialogFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable("user", user);
        roomsCreateDialogFragment.setArguments(args);

        return roomsCreateDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        mUser = (User) getArguments().getSerializable("user");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "onCreateDialog()");
        mRoomsActivity = (RoomsActivity) getActivity();
        AlertDialog.Builder builder = mRoomsActivity.getThemeAlertDialogBuilder();
        initializeViews();
        TextView titleView = createCustomTitleTextView(getActivity());
        builder
                .setCustomTitle(titleView)
                .setView(mView)
                .setPositiveButton("Создать", (dialog, id) -> {})
                .setNegativeButton("Отмена", (dialog, id) -> dialog.cancel());

        mAlertDialog = builder.create();
        mAlertDialog.show();
        mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            if (validate(mRoomName.getText().toString(), mRoomPassword.getText().toString())) {
                createRoom(
                        mUser,
                        mRoomName.getText().toString(),
                        mRoomPassword.getText().toString(),
                        mIsEmptyPassword.isChecked(),
                        mDiscreteSeekBar.getProgress());
            }
        });
        return mAlertDialog;
    }

    private void initializeViews() {
        Log.d(TAG, "initializeViews()");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        mView = inflater.inflate(R.layout.dialog_create_room, null);
        mRoomName = (EditText) mView.findViewById(R.id.dialog_add_room_name);
        mRoomPassword = (EditText) mView.findViewById(R.id.dialog_add_room_password);
        mIsEmptyPassword = (CheckBox) mView.findViewById(R.id.dialog_add_is_empty_pass);
        mDiscreteSeekBar = (DiscreteSeekBar) mView.findViewById(R.id.dialog_add_room_seek_bar);

        mIsEmptyPassword.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                mRoomPassword.setVisibility(View.INVISIBLE);
                mRoomPassword.setClickable(false);
            }
            else {
                mRoomPassword.setVisibility(View.VISIBLE);
                mRoomPassword.setClickable(true);
            }
        });
    }

    private TextView createCustomTitleTextView(Context context) {
        TextView title = new TextView(context);
        title.setText("Создать комнату");
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.WHITE);
        title.setTextSize(20);
        return title;
    }


    private void createRoom(User owner, String roomName, String roomPassword,
                            boolean isEmptyPassword, int maxCountUsers) {
        Log.d(TAG, "createRoom()");
        final ProgressDialog progressDialog = createProgressDialog();
        progressDialog.show();

        RoomRequest roomRequest = new RoomRequest(roomName,
                roomPassword, isEmptyPassword, owner,
                maxCountUsers, new LinkedList<>(), new LinkedList<>());

        mRoomApi.createRoom(roomRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(roomResponse -> {
                    Log.d(TAG, roomResponse.getStatus().toString());
                    progressDialog.dismiss();
                    switch (roomResponse.getStatus()) {
                        case ROOM_OK:
                            mRoomsActivity.prepareRoomData();
                            mAlertDialog.dismiss();
                            break;
                        case ROOM_NAME_EXIST:
                            mRoomName.setError("Комната с таким названием уже существует");
                            break;
                        default:onLoadFailed("Неизвестная ошибка...");
                    }

                }, throwable -> {
                    Log.d(TAG, throwable.getMessage(), throwable);
                    progressDialog.dismiss();
                    onLoadFailed("Ошибка подключения");
                });

    }

    @NonNull
    private ProgressDialog createProgressDialog() {
        Log.d(TAG, "createProgressDialog()");
        final ProgressDialog progressDialog = mRoomsActivity.getThemeProgressDialog();
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Создаём...");
        return progressDialog;
    }

    private void onLoadFailed(String s) {
        Log.d(TAG, "onLoadFailed()");
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

    private boolean validate(String roomName, String roomPassword) {
        Log.d(TAG, "validate()");
        boolean valid = true;

        String resultName = nameValidate(roomName);
        String resultPassword = null;
        if (!mIsEmptyPassword.isChecked()) {
            resultPassword = passwordValidate(roomPassword);
        }
        if (resultName != null || resultPassword != null)
            valid = false;

        mRoomName.setError(resultName);
        mRoomPassword.setError(resultPassword);

        return valid;
    }


    private String nameValidate(String name) {
        Log.d(TAG, "nameValidate()");
        if (name.length() < ROOM_NAME_MIN_LENGTH)
            return "Название комнаты не может быть меньше" + " " + ROOM_NAME_MIN_LENGTH;

        if (name.length() > ROOM_NAME_MAX_LENGTH)
            return "Название комнаты не может быть больше" + " " + ROOM_NAME_MAX_LENGTH;

        if (!name.matches(ROOM_NAME_NOT_MIXED_ALPHABET_PATTERN))
            return "Нельзя смешивать алфавиты";

        return null;
    }

    private String passwordValidate(String password) {
        Log.d(TAG, "passwordValidate()");
        if (password.length() < ROOM_PASSWORD_MIN_LENGTH)
            return "Пароль не может быть меньше чем" + " " + ROOM_PASSWORD_MIN_LENGTH;

        if (password.length() > ROOM_PASSWORD_MAX_LENGTH)
            return "Пароль не может быть больше чем" + " " + ROOM_PASSWORD_MAX_LENGTH;

        return  null;
    }


}
