package com.celestialapps.roomchats.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by Sergey on 08.05.2017.
 */

@SuppressLint("Registered")
public class ThemeActivity extends AppCompatActivity {

    private final String TAG = "ThemeActivity";
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        setTheme(getAppThemePreferences());
    }



    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private int getAppThemePreferences() {
        if (sharedPreferences == null)
            return R.style.AppTheme_Red;

        return sharedPreferences.getInt("app_theme", R.style.AppTheme_Red);
    }

    public ProgressDialog getThemeProgressDialog() {
        if (sharedPreferences == null)
            return new ProgressDialog(this, R.style.AppTheme_Red_ProgressDialog);

        int progressDialogTheme = sharedPreferences.getInt("progress_dialog_theme",
                R.style.AppTheme_Red_ProgressDialog);

        return new ProgressDialog(this, progressDialogTheme);
    }

    public int getThemeTextColor() {
        TypedValue typedValue = new TypedValue();

        TypedArray a = obtainStyledAttributes(typedValue.data, new int[] {android.R.attr.textColor});
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }

    public AlertDialog.Builder getThemeAlertDialogBuilder() {
        if (sharedPreferences == null)
            return new AlertDialog.Builder(this, R.style.AppTheme_Red_AlertDialog);

        int alertDialogTheme = sharedPreferences.getInt("alert_dialog_theme",
                R.style.AppTheme_Red_AlertDialog);

        return new AlertDialog.Builder(this, alertDialogTheme);
    }

}
