package com.celestialapps.roomchats.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.celestialapps.roomchats.activity.adapters.RoomsAdapter;
import com.celestialapps.roomchats.activity.fragments.RoomsCreateDialogFragment;
import com.celestialapps.roomchats.model.Room;
import com.celestialapps.roomchats.model.User;
import com.celestialapps.roomchats.networking.RoomChatsService;
import com.celestialapps.roomchats.networking.interfaces.RoomApi;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RoomsActivity extends ToolbarActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    private final String TAG = "RoomsActivity";

    private List<Room> mRoomList = new ArrayList<>();
    private RoomApi roomApi = RoomChatsService.getRoomApi();

    private RoomsAdapter mRoomsAdapter;

    @BindView(R.id.swipeRefreshLayout) SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.rooms_recycler_view) RecyclerView mRoomsRecyclerView;
    @BindView(R.id.button_create_room) Button mCreateRoomButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);
        ButterKnife.bind(this);
        roomsInitialize();
        mCreateRoomButton.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
        prepareRoomData();
    }

    private void roomsInitialize() {
        Log.d(TAG, "roomsInitialize()");
        mRoomsAdapter = new RoomsAdapter(this, mRoomList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRoomsRecyclerView.setLayoutManager(layoutManager);
        mRoomsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRoomsRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRoomsRecyclerView.setAdapter(mRoomsAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }



    public void prepareRoomData() {
        Log.d(TAG, "prepareRoomData()");
        ProgressDialog progressDialog = createProgressDialog();
        progressDialog.show();

        roomApi.getRooms()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(roomListResponse -> {
                    Log.d(TAG, roomListResponse.getStatus().toString());
                    progressDialog.dismiss();
                    switch (roomListResponse.getStatus()) {
                        case ROOM_OK:
                                mRoomList.clear();
                                mRoomList.addAll(roomListResponse.getRoomList());
                                mRoomsAdapter.notifyDataSetChanged();
                                mRoomsRecyclerView.setAdapter(mRoomsAdapter);
                            break;
                        default: onLoadFailed(getString(R.string.rooms_connection_error));
                    }
                }, throwable -> {
                    Log.d(TAG, throwable.getMessage(), throwable);
                    progressDialog.dismiss();
                    onLoadFailed(getString(R.string.rooms_connection_error));
                });
    }

    @NonNull
    private ProgressDialog createProgressDialog() {
        Log.d(TAG, "createProgressDialog()");
        final ProgressDialog progressDialog = getThemeProgressDialog();
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.rooms_wait));
        return progressDialog;
    }

    private void onLoadFailed(String s) {
        Log.d(TAG, "onLoadFailed()");
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        new Handler().postDelayed(this::recreate, 0);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick()");
        DialogFragment dialogFragment = RoomsCreateDialogFragment.newInstance((User) getIntent().getSerializableExtra("user"));
        dialogFragment.show(getSupportFragmentManager(), "rooms_add_dialog");
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        prepareRoomData();
    }
}
