package com.celestialapps.roomchats.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.celestialapps.roomchats.networking.RoomChatsService;
import com.celestialapps.roomchats.networking.interfaces.UserApi;
import com.celestialapps.roomchats.networking.request.UserRequest;

import java.util.LinkedList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SignUpActivity extends ToolbarActivity {
    private final String TAG = "SignUpActivity";

    private final int USER_LOGIN_MIN_LENGTH = 3;
    private final int USER_LOGIN_MAX_LENGTH = 12;
    private final String USER_LOGIN_NOT_MIXED_ALPHABET_PATTERN = "^([a-zA-Z0-9-_\\\\.]*)|([а-яА-Я0-9-_\\\\.]*)$";

    private final int USER_PASSWORD_MIN_LENGTH = 8;
    private final int USER_PASSWORD_MAX_LENGTH = 16;
    private final String[] EASY_PASSWORDS = {
            "12344321", "12345678", "87654321", "43211234",
            "qwertyui", "iuytrewq", "1qaz1qaz", "zaq1zaq1",
            "1234qwer", "rewq4321", "1йфя1йфя", "йцукенгш"};

    private UserApi userApi = RoomChatsService.getUserApi();

    @BindView(R.id.input_login) EditText mLoginText;
    @BindView(R.id.input_email) EditText mEmailText;
    @BindView(R.id.input_password) EditText mPasswordText;
    @BindView(R.id.input_re_enter_password) EditText mReEnterPasswordText;
    @BindView(R.id.btn_sign_up) Button mSignUpButton;
    @BindView(R.id.link_login) TextView mLoginLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        mSignUpButton.setOnClickListener(v -> signUp());

        mLoginLink.setOnClickListener(v -> {
            // Finish the registration screen and return to the Login activity
            Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        });
    }

    private void signUp() {
        Log.d(TAG, "SignUp()");

        if (!validate()) {
            onSignUpFailed(getString(R.string.sign_up_fill_fields_correctly));
            return;
        }

        mSignUpButton.setEnabled(false);

        final String login = mLoginText.getText().toString();
        final String password = mPasswordText.getText().toString();
        final String email = mEmailText.getText().toString();


        createUser(login, password, email);
    }

    private void createUser(String login, String password, String email) {
        Log.d(TAG, "createUser()");
        final ProgressDialog progressDialog = createProgressDialog();
        progressDialog.show();

        userApi.createUser(new UserRequest(login, password, email, new LinkedList<>(), null))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userResponse -> {
                    Log.d(TAG, userResponse.getStatus().toString());
                    progressDialog.dismiss();
                    switch (userResponse.getStatus()) {
                        case USER_OK:
                            onSignUpSuccess();
                            break;
                        case USER_LOGIN_EXIST:
                            onSignUpFailed(getString(R.string.sign_up_login_already_exists));
                            break;
                        case USER_EMAIL_EXIST:
                            onSignUpFailed(getString(R.string.sign_up_email_already_exists));
                            break;
                        default: onSignUpFailed(getString(R.string.sign_up_unknown_error_during_registration));
                    }
                }, throwable -> {
                    Log.d(TAG, throwable.getMessage(), throwable);
                    progressDialog.dismiss();
                    onSignUpFailed(getString(R.string.sign_up_connection_error));
                });
    }

    @NonNull
    private ProgressDialog createProgressDialog() {
        Log.d(TAG, "createProgressDialog()");
        final ProgressDialog progressDialog = getThemeProgressDialog();
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.sign_up_creating_an_account));
        return progressDialog;
    }


    private void onSignUpSuccess() {
        Log.d(TAG, "onSignUpSuccess()");
        mSignUpButton.setEnabled(true);
        setResult(RESULT_OK, new Intent()
                        .putExtra("login", mLoginText.getText().toString())
                        .putExtra("password", mPasswordText.getText().toString()));
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void onSignUpFailed(String message) {
        Log.d(TAG, "onSignUpFailed()");
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();

        mSignUpButton.setEnabled(true);
    }

    private boolean validate() {
        Log.d(TAG, "validate()");
        boolean valid = true;

        String login = mLoginText.getText().toString();
        String email = mEmailText.getText().toString();
        String password = mPasswordText.getText().toString();
        String reEnterPassword = mReEnterPasswordText.getText().toString();

        String resultLogin = loginValidate(login);
        String resultEmail = emailValidate(email);
        String resultPassword = passwordValidate(password);
        String resultReEnterPassword = rePasswordValidate(password, reEnterPassword);

        if (resultLogin != null
                || resultEmail != null
                || resultPassword != null
                || resultReEnterPassword != null) valid = false;

        mLoginText.setError(resultLogin);
        mEmailText.setError(resultEmail);
        mPasswordText.setError(resultPassword);
        mReEnterPasswordText.setError(resultReEnterPassword);

        return valid;
    }

    private String loginValidate(String login) {
        Log.d(TAG, "loginValidate()");
        if (login.length() < USER_LOGIN_MIN_LENGTH)
            return getString(R.string.sign_up_length_nick_name_can_not_be_less_than) + " " + USER_LOGIN_MIN_LENGTH;

        if (login.length() > USER_LOGIN_MAX_LENGTH)
            return getString(R.string.sign_up_length_nick_name_can_not_be_greater_than) + " " + USER_LOGIN_MAX_LENGTH;

        if (!login.matches(USER_LOGIN_NOT_MIXED_ALPHABET_PATTERN))
            return getString(R.string.sign_up_can_not_mix_alphabets);

        if (Character.isDigit(login.charAt(0)))
            return getString(R.string.sign_up_login_can_not_begin_digit);

        return null;
    }

    private String passwordValidate(String password) {
        Log.d(TAG, "passwordValidate()");
        if (password.length() < USER_PASSWORD_MIN_LENGTH)
            return getString(R.string.sign_up_password_can_not_be_less_than) + " " + USER_PASSWORD_MIN_LENGTH;

        if (password.length() > USER_PASSWORD_MAX_LENGTH)
            return getString(R.string.sign_up_password_can_not_be_greater_than) + " " + USER_PASSWORD_MAX_LENGTH;

        for (String easyPassword: EASY_PASSWORDS) {
            if (password.equals(easyPassword))
                return getString(R.string.sign_up_password_too_easy);
        }


        for (int i = 1; i < password.length(); i++) {
            if (password.charAt(0) != password.charAt(i)) {
                return null;
            }
        }
        return  getString(R.string.sign_up_password_too_easy);
    }

    private String rePasswordValidate(String password, String rePassword) {
        Log.d(TAG, "rePasswordValidate()");
        if (!password.equals(rePassword))
            return getString(R.string.sign_up_passwords_not_equals);

        return null;
    }

    private String emailValidate(String email) {
        Log.d(TAG, "emailValidate()");
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            return getString(R.string.invalid_email_address);

        return null;
    }

}