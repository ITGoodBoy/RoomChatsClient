package com.celestialapps.roomchats.activity.adapters;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.celestialapps.roomchats.activity.R;
import com.celestialapps.roomchats.activity.RoomActivity;
import com.celestialapps.roomchats.activity.fragments.RoomsSignInDialogFragment;
import com.celestialapps.roomchats.model.Room;
import com.celestialapps.roomchats.model.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sergey on 24.04.2017.
 */

public class RoomsAdapter extends RecyclerView.Adapter<RoomsAdapter.RoomViewHolder> {
    private final String TAG = "RoomsAdapter";

    private AppCompatActivity context;
    private List<Room> roomList;

    public RoomsAdapter(AppCompatActivity context, List<Room> roomList) {
        Log.d(TAG, "RoomsAdapter()");
        this.context = context;
        this.roomList = roomList;
    }

    public class RoomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private User user;

        @BindView(R.id.room_name) AppCompatTextView mRoomName;
        @BindView(R.id.room_owner) AppCompatTextView mRoomOwner;
        @BindView(R.id.room_current_count_users) AppCompatTextView mRoomCurrentCountUsers;
        @BindView(R.id.room_max_count_users) AppCompatTextView mRoomMaxCountUsers;
        @BindView(R.id.imageView) AppCompatImageView mImageView;

        public RoomViewHolder(View view) {
            super(view);
            Log.d(TAG, "RoomViewHolder()");
            ButterKnife.bind(RoomViewHolder.this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClickViewHolder()");
            user = (User) context.getIntent().getSerializableExtra("user");
            Room room = roomList.get(getAdapterPosition());

            if (room.isEmptyPassword()) {
                user.setCurrentRoom(room);
                context.startActivity(new Intent(context, RoomActivity.class)
                        .putExtra("user", user));
            } else {
                DialogFragment dialogFragment = RoomsSignInDialogFragment.newInstance(user, room);
                dialogFragment.show(context.getSupportFragmentManager(), "room_dialog_sign_in");
            }
        }
    }


    @Override
    public RoomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder()");
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.room_list_row, parent, false);

        return new RoomViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RoomViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder()");
        Room room = roomList.get(position);
        holder.mRoomName.setText(context.getString(R.string.rooms_adapter_room)
                + " "
                + room.getName());
        holder.mRoomOwner.setText(context.getString(R.string.owner)
                + " "
                + room.getOwner().getLogin());
        holder.mRoomCurrentCountUsers.setText(context.getString(R.string.rooms_adapter_now_in_room)
                + " "
                + room.getUsersInRoom().size());
        holder.mRoomMaxCountUsers.setText(context.getString(R.string.rooms_adapter_max_in_room)
                + " "
                + room.getMaxCountUsers());
        if (room.isEmptyPassword()) {
            holder.mImageView.setBackgroundResource(R.drawable.ic_unlock);
        } else {
            holder.mImageView.setBackgroundResource(R.drawable.ic_lock);
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount()");
        return roomList.size();
    }


}
