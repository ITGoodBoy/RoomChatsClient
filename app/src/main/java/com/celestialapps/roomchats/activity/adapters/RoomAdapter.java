package com.celestialapps.roomchats.activity.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.celestialapps.roomchats.activity.R;

import java.util.List;

import me.himanshusoni.chatmessageview.ChatMessageView;

/**
 * Created by Sergey on 05.05.2017.
 */

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.RoomHolder>{
    private final String TAG = "RoomAdapter";

    private List<String> mMessages;
    private Context mContext;

    public RoomAdapter(Context context, List<String> data) {
        mContext = context;
        mMessages = data;
    }

    class RoomHolder extends RecyclerView.ViewHolder {
        TextView tvMessage;

        RoomHolder(View itemView) {
            super(itemView);
            tvMessage = (TextView) itemView.findViewById(R.id.text1);
        }
    }


    @Override
    public RoomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RoomHolder(LayoutInflater.from(mContext).inflate(R.layout.activity_room_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RoomHolder holder, int position) {
        String message = mMessages.get(position);
        holder.tvMessage.setText(message);
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    public List<String> getMessages() {
        return mMessages;
    }
}
