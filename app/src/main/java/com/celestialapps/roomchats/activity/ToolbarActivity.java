package com.celestialapps.roomchats.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;

import com.celestialapps.roomchats.activity.preferences.SettingsPreferencesActivity;
import com.celestialapps.roomchats.activity.preferences.SettingsPreferencesFragment;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;

/**
 * Created by Sergey on 09.05.2017.
 */

@SuppressLint("Registered")
public class ToolbarActivity extends LanguageActivity {

    private final String TAG = "ToolbarActivity";
    private final int RESTART_REQUEST = 1;

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toolbarInitialize();
    }

    private void toolbarInitialize() {
        Log.d(TAG, "toolbarInitialize()");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) return;

        Log.d(TAG, "toolbar notNull");
        TypedValue a = new TypedValue();
        getTheme().resolveAttribute(android.R.attr.windowBackground, a, true);
        int backgroundColor = a.data;

        Drawer drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withDisplayBelowStatusBar(true)
                .withTranslucentStatusBar(true)
                .withSliderBackgroundColor(backgroundColor)
                .addDrawerItems(
                        new SecondaryDrawerItem().withName(R.string.toolbar_settings)
                )
                .withOnDrawerItemClickListener((view, position, iDrawerItem) -> {
                    switch (position) {
                        case 0:
                            startActivityForResult(new Intent(this, SettingsPreferencesActivity.class), RESTART_REQUEST);
                            break;
                    }
                    return true;
                })
                .build();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == SettingsPreferencesFragment.RESTART_RESULT) {
            new Handler().postDelayed(this::recreate, 0);
            startActivityForResult(new Intent(this, SettingsPreferencesActivity.class), RESTART_REQUEST);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }
}
