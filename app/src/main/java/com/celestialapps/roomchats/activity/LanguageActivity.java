package com.celestialapps.roomchats.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.celestialapps.roomchats.activity.ThemeActivity;

import java.util.Locale;

/**
 * Created by Sergey on 09.05.2017.
 */

@SuppressLint("Registered")
public class LanguageActivity extends ThemeActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLanguageApp(getLanguagePreferences());
    }

    private void setLanguageApp(String language) {
        if (Locale.getDefault().getLanguage().equals(language)) return;
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());
        recreate();
    }

    private String getLanguagePreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences("settings", Context.MODE_PRIVATE);
        if (sharedPreferences == null)
            return Locale.getDefault().getLanguage();

        return sharedPreferences.getString("language", Locale.getDefault().getLanguage());
    }
}
