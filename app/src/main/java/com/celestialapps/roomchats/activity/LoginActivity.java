package com.celestialapps.roomchats.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.content.Intent;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.celestialapps.roomchats.model.User;
import com.celestialapps.roomchats.networking.RoomChatsService;
import com.celestialapps.roomchats.networking.interfaces.UserApi;


import butterknife.BindView;
import butterknife.ButterKnife;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class LoginActivity extends ToolbarActivity {
    private final String TAG = "LoginActivity";
    private final UserApi userApi = RoomChatsService.getUserApi();
    private final int REQUEST_SIGN_UP = 0;

    @BindView(R.id.toolbar) Toolbar mToolbar;
    @BindView(R.id.input_login) EditText mLoginText;
    @BindView(R.id.input_password) EditText mPasswordText;
    @BindView(R.id.btn_login) AppCompatButton mLoginButton;
    @BindView(R.id.link_sign_up) TextView mSignUpLink;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mLoginButton.setOnClickListener(v -> login());

        mSignUpLink.setOnClickListener(v -> {
            // Start the SignUp activity
            Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
            startActivityForResult(intent, REQUEST_SIGN_UP);
            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            finish();
        });
    }


    public void login() {
        Log.d(TAG, "login()");

        if (!validate()) {
            onLoginFailed(getString(R.string.login_fields_can_not_be_empty));
            return;
        }

        mLoginButton.setEnabled(false);

        String nickName = mLoginText.getText().toString();
        String password = mPasswordText.getText().toString();

        authorizeUser(nickName, password);

    }


    private void authorizeUser(final String nickName, final String password) {
        Log.d(TAG, "authorizeUser()");
        ProgressDialog progressDialog = createProgressDialog();
        progressDialog.show();

        userApi.authorizeUser(nickName, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userResponse -> {
                    Log.d(TAG, userResponse.getStatus().toString());
                    progressDialog.dismiss();
                    switch (userResponse.getStatus()) {
                        case USER_OK:
                            User user = userResponse.getUser();
                            user.setPassword(password);
                            onLoginSuccess(user);
                            break;
                        case USER_LOGIN_NOT_EXIST:
                            onLoginFailed(getString(R.string.login_login_not_found));
                            break;
                        case USER_PASSWORD_NOT_EQUALS:
                            onLoginFailed(getString(R.string.login_incorrect_password));
                            mPasswordText.setText("");
                            break;
                        default: onLoginFailed(getString(R.string.login_unknown_error));
                    }
                }, throwable -> {
                    Log.d(TAG, throwable.getMessage(), throwable);
                    progressDialog.dismiss();
                    onLoginFailed(getString(R.string.login_connection_error));
                });
    }


    @NonNull
    private ProgressDialog createProgressDialog() {
        Log.d(TAG, "createProgressDialog()");
        final ProgressDialog progressDialog = getThemeProgressDialog();
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(getString(R.string.login_authentication));
        return progressDialog;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult()" + resultCode);
        if (requestCode == REQUEST_SIGN_UP) {
            if (resultCode == RESULT_OK) {
                mLoginText.setText(data.getStringExtra("login"));
                mPasswordText.setText(data.getStringExtra("password"));
            }
        }
    }


    public void onLoginSuccess(User user) {
        Log.d(TAG, "onLoginSuccess()");
        Intent intent = new Intent(LoginActivity.this, RoomsActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
        finish();
    }


    public void onLoginFailed(String message) {
        Log.d(TAG, "onLoginFailed()");
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
        mLoginButton.setEnabled(true);
    }


    public boolean validate() {
        Log.d(TAG, "validate()");
        boolean valid = true;

        String nickName = mLoginText.getText().toString();
        String password = mPasswordText.getText().toString();

        if (nickName.isEmpty()) {
            mLoginText.setError(getString(R.string.login_login_can_not_be_empty));
            valid = false;
        } else {
            mLoginText.setError(null);
        }

        if (password.isEmpty()) {
            mPasswordText.setError(getString(R.string.login_password_can_not_be_empty));
            valid = false;
        } else {
            mPasswordText.setError(null);
        }

        return valid;
    }

}
