package com.celestialapps.roomchats.activity.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.celestialapps.roomchats.activity.R;
import com.celestialapps.roomchats.activity.RoomActivity;
import com.celestialapps.roomchats.activity.RoomsActivity;
import com.celestialapps.roomchats.model.Room;
import com.celestialapps.roomchats.model.User;
import com.celestialapps.roomchats.networking.RoomChatsService;
import com.celestialapps.roomchats.networking.interfaces.RoomApi;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Sergey on 24.04.2017.
 */

public class RoomsSignInDialogFragment extends DialogFragment {
    private static final String TAG = "RoomsSignInDialogFragme";

    private User user;
    private Room room;
    private RoomApi roomApi = RoomChatsService.getRoomApi();
    private View view;
    private RoomsActivity roomsActivity;
    private AlertDialog alertDialog;


    public static RoomsSignInDialogFragment newInstance(User user, Room room) {
        Log.d(TAG, "newInstance()");
        RoomsSignInDialogFragment roomsSignInDialogFragment = new RoomsSignInDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable("user", user);
        args.putSerializable("room", room);
        roomsSignInDialogFragment.setArguments(args);

        return roomsSignInDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        user = (User) getArguments().getSerializable("user");
        room = (Room) getArguments().getSerializable("room");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Log.d(TAG, "onCreateDialog()");
        roomsActivity = (RoomsActivity) getActivity();
        AlertDialog.Builder builder = roomsActivity.getThemeAlertDialogBuilder();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.dialog_sign_in_room, null);
        final EditText roomPassword = (EditText) view.findViewById(R.id.room_password);
        TextView textView = createCustomTitleTextView(getActivity());
        builder
                .setCustomTitle(textView)
                .setView(view)
                .setPositiveButton("Войти", (dialog, id) -> {})
                .setNegativeButton("Отмена", (dialog, id) -> dialog.cancel());

        alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setOnClickListener(v -> authorizeToRoom(room.getName(), roomPassword.getText().toString()));
        return alertDialog;
    }

    private void authorizeToRoom(final String roomName, final String roomPassword) {
        Log.d(TAG, "authorizeToRoom()");
        final ProgressDialog progressDialog = createProgressBar();
        progressDialog.show();

        roomApi.authorizeRoom(roomName, roomPassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(roomResponse -> {
                    Log.d(TAG, roomResponse.getStatus().toString());
                    progressDialog.dismiss();
                    switch (roomResponse.getStatus()) {
                        case ROOM_OK:
                            room.setPassword(roomPassword);
                            user.setCurrentRoom(room);
                            alertDialog.dismiss();
                            roomsActivity
                                    .startActivity(new Intent(roomsActivity, RoomActivity.class)
                                    .putExtra("user", user));
                            break;
                        case ROOM_NAME_NOT_EXIST:
                            alertDialog.dismiss();
                            break;
                        case ROOM_PASSWORD_NOT_EQUALS:
                            EditText password = (EditText) view.findViewById(R.id.room_password);
                            password.setText("");
                            password.setError("Неверный пароль");
                            break;
                            default:onLoadFailed("Неизвестная ошибка");
                    }
                }, throwable -> {
                    Log.d(TAG, throwable.getMessage(), throwable);
                    progressDialog.dismiss();
                    onLoadFailed("Ошибка подключения");
                });
    }

    private TextView createCustomTitleTextView(Context context) {
        TextView title = new TextView(context);
        title.setText("Вход в комнату");
        title.setPadding(10, 10, 10, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.WHITE);
        title.setTextSize(20);
        return title;
    }

    @NonNull
    private ProgressDialog createProgressBar() {
        Log.d(TAG, "createProgressBar()");
        final ProgressDialog progressDialog = roomsActivity.getThemeProgressDialog();
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Входим...");
        return progressDialog;
    }

    private void onLoadFailed(String s) {
        Log.d(TAG, "onLoadFailed()");
        Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
    }

}
