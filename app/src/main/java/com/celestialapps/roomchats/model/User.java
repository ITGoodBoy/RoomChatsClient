package com.celestialapps.roomchats.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Sergey on 15.04.2017.
 */

public class User implements Serializable {

    @Expose
    private long id;
    @Expose
    private String login;
    @Expose
    private String password;
    @Expose
    private String email;
    @Expose
    private List<Room> ownerRooms;
    @Expose
    private Room currentRoom;

    public User() {

    }

    public User(long id) {
        this.id = id;
    }

    public User(String login, String password, String email, List<Room> ownerRooms, Room currentRoom) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.ownerRooms = ownerRooms;
        this.currentRoom = currentRoom;
    }

    public User(long id, String login, String password, String email, List<Room> ownerRooms, Room currentRoom) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.email = email;
        this.ownerRooms = ownerRooms;
        this.currentRoom = currentRoom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Room> getOwnerRooms() {
        return ownerRooms;
    }

    public void setOwnerRooms(List<Room> ownerRooms) {
        this.ownerRooms = ownerRooms;
    }

    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", ownerRooms=" + ownerRooms +
                ", currentRoom=" + currentRoom +
                '}';
    }
}
