package com.celestialapps.roomchats.networking.response;

import com.celestialapps.roomchats.model.User;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Sergey on 01.05.2017.
 */
public class UserListResponse extends UserResponse {

    @Expose
    private final List<User> userList;

    public UserListResponse(Status status) {
        super(status);
        this.userList = null;
    }

    public UserListResponse(Status status, List<User> userList) {
        super(status);
        this.userList = userList;
    }

    public List<User> getUserList() {
        return userList;
    }

    @Deprecated
    @Override
    public User getUser() {
        throw new UnsupportedOperationException();
    }
}
