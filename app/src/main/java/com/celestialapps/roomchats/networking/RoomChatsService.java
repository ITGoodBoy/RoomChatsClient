package com.celestialapps.roomchats.networking;

import android.annotation.SuppressLint;
import android.app.Application;

import com.celestialapps.roomchats.networking.interfaces.RoomApi;
import com.celestialapps.roomchats.networking.interfaces.UserApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


@SuppressLint("Registered")
public class RoomChatsService extends Application{

    private static final String SERVER_BASE_URL = "http://192.168.0.101:8080/";
    private static Retrofit retrofit = null;
    private static UserApi userApi;
    private static RoomApi roomApi;


    private static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(SERVER_BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                            .excludeFieldsWithoutExposeAnnotation()
                            .create()))
                    .build();
        }
        return retrofit;
    }

    public static UserApi getUserApi() {
        if (userApi == null)
            userApi = getClient().create(UserApi.class);
        return userApi;
    }

    public static RoomApi getRoomApi() {
        if (roomApi == null)
            roomApi = getClient().create(RoomApi.class);
        return roomApi;
    }

}
