package com.celestialapps.roomchats.networking.interfaces;



import com.celestialapps.roomchats.model.User;
import com.celestialapps.roomchats.networking.request.UserRequest;
import com.celestialapps.roomchats.networking.response.UserListResponse;
import com.celestialapps.roomchats.networking.response.UserResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;


public interface UserApi {

    @GET("users/get-by-id")
    Observable<UserResponse> getUserById(@Query("id") long id);

    @GET("users/get-by-login")
    Observable<UserResponse> getUserByLogin(@Query("login") String login);

    @GET("users/get")
    Observable<UserListResponse> getUsers();

    @GET("users/authorize")
    Observable<UserResponse> authorizeUser(@Query("login") String login, @Query("password") String password);

    @PUT("users/create")
    Observable<UserResponse> createUser(@Body UserRequest userRequest);

    @POST("users/update")
    Observable<UserResponse> updateUser(@Body UserRequest userRequest);



}
