package com.celestialapps.roomchats.networking.interfaces;

import com.celestialapps.roomchats.networking.request.RoomRequest;
import com.celestialapps.roomchats.networking.response.RoomListResponse;
import com.celestialapps.roomchats.networking.response.RoomResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by Sergey on 19.04.2017.
 */

public interface RoomApi {


    @GET("rooms/get-by-id")
    Observable<RoomResponse> getRoomById(@Query("id") long id);

    @GET("rooms/get-by-name")
    Observable<RoomResponse> getRoomByName(@Query("name") String name);

    @GET("rooms/get-by-max-count-users")
    Observable<RoomListResponse> getRoomsByMaxCountUsers(@Query("maxCountUsers") int maxCountUsers);

    @GET("rooms/get")
    Observable<RoomListResponse> getRooms();

    @GET("rooms/authorize")
    Observable<RoomResponse> authorizeRoom(@Query("name") String name, @Query("password") String password);

    @PUT("rooms/create")
    Observable<RoomResponse> createRoom(@Body RoomRequest roomRequest);

    @POST("rooms/update")
    Observable<RoomResponse> updateRoom(@Body RoomRequest roomRequest);

    @DELETE("rooms/delete")
    Observable<RoomResponse> deleteRoom(@Body RoomRequest roomRequest);
}
