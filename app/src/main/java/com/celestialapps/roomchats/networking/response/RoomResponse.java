package com.celestialapps.roomchats.networking.response;

import com.celestialapps.roomchats.model.Room;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Sergey on 01.05.2017.
 */
public class RoomResponse {

    @Expose
    private final Status status;
    @Expose
    private final Room room;

    public enum Status {
        ROOM_OK,
        ROOM_EMPTY_FIELDS,
        ROOM_ID_NOT_EXIST,
        ROOM_NAME_EXIST,
        ROOM_NAME_NOT_EXIST,
        ROOM_NAME_MIXED_ALPHABET,
        ROOM_NAME_LENGTH_SHORT,
        ROOM_NAME_LENGTH_LONG,
        ROOM_PASSWORD_NOT_EQUALS,
        ROOM_PASSWORD_LENGTH_SHORT,
        ROOM_PASSWORD_LENGTH_LONG,
        ROOM_OWNER_LOGIN_NOT_EXIST,
        ROOM_OWNER_PASSWORD_NOT_EQUALS,
        ROOM_THIS_MIN_COUNT_USERS_NOT_EXIST,
        ROOM_MIN_COUNT_USERS_BELOW_MINIMUM,
        ROOM_THIS_MAX_COUNT_USERS_NOT_EXIST,
        ROOM_MAX_COUNT_USERS_ABOVE_MAXIMUM
    }

    public RoomResponse(Status status) {
        this.status = status;
        this.room = null;
    }

    public RoomResponse(Status status, Room room) {
        this.status = status;
        this.room = room;
    }


    public Status getStatus() {
        return status;
    }

    public Room getRoom() {
        return room;
    }

}
