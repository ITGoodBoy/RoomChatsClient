package com.celestialapps.roomchats.networking.request;

import com.celestialapps.roomchats.model.User;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Sergey on 01.05.2017.
 */
public class RoomRequest {

    @Expose
    private String name;
    @Expose
    private String password;
    @Expose
    private boolean isEmptyPassword;
    @Expose
    private User owner;
    @Expose
    private int maxCountUsers;
    @Expose
    private List<String> usersInRoom;
    @Expose
    private List<String> messageList;

    public RoomRequest() {
    }

    public RoomRequest(String name, String password, boolean isEmptyPassword, User owner,
                       int maxCountUsers, List<String> usersInRoom, List<String> messageList) {
        this.name = name;
        this.password = password;
        this.isEmptyPassword = isEmptyPassword;
        this.owner = owner;
        this.maxCountUsers = maxCountUsers;
        this.usersInRoom = usersInRoom;
        this.messageList = messageList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public int getMaxCountUsers() {
        return maxCountUsers;
    }

    public void setMaxCountUsers(int maxCountUsers) {
        this.maxCountUsers = maxCountUsers;
    }

    public List<String> getUsersInRoom() {
        return usersInRoom;
    }

    public void setUsersInRoom(List<String> usersInRoom) {
        this.usersInRoom = usersInRoom;
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    public boolean isEmptyPassword() {
        return isEmptyPassword;
    }

    public void setEmptyPassword(boolean emptyPassword) {
        isEmptyPassword = emptyPassword;
    }

}
