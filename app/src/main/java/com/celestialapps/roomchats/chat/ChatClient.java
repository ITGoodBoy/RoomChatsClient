package com.celestialapps.roomchats.chat;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.celestialapps.roomchats.activity.adapters.RoomAdapter;
import com.celestialapps.roomchats.chat.common.Message;
import com.celestialapps.roomchats.chat.common.MessageType;
import com.celestialapps.roomchats.model.User;

import java.util.List;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;


/**
 * Created by Sergey on 13.04.2017.
 */
public class ChatClient {
    private final String TAG = "ChatClient";

    private final String HOST = "192.168.0.101";
    private final int PORT = 8081;

    private User user;
    private RecyclerView roomMessageList;

    private Channel channel;
    private Thread thread;

    public ChatClient(User user, RecyclerView roomMessageList) {
        this.user = user;
        this.roomMessageList = roomMessageList;
    }

    public void sendMessage(String text) {
        Log.d(TAG, "sendMessage()");
        if (channel == null || !channel.isActive()) return;
        channel.writeAndFlush(new Message(MessageType.TEXT, user.getLogin(), user.getCurrentRoom().getName(), text));
    }

    public void run() {
        Log.d(TAG, "run()");
            thread = new Thread(() -> {
                EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
                try {
                    Bootstrap bootstrap = new Bootstrap()
                            .group(eventLoopGroup)
                            .channel(NioSocketChannel.class)
                            .handler(new ChatClientInitializer(user, roomMessageList));

                    channel = bootstrap.connect(HOST, PORT).sync().channel();
                    while (!thread.isInterrupted());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    eventLoopGroup.shutdownGracefully();
                }
            });
        thread.start();
    }

    public boolean isRunning() {
        return !thread.isInterrupted();
    }


    public void stop() {
        Log.d(TAG, "stop()");
        if (thread != null) thread.interrupt();
    }
}
