package com.celestialapps.roomchats.chat.common;


 public enum MessageType {
    TEXT,
    NAME_REQUEST,
    NAME_ACCEPTED,
    USER_NAME,
    USER_ADDED,
    USER_REMOVED,
    ADMIN_DELETE_USER,
    USER_NOT_FOUND,
    USER_GROUP_REQUEST,
    USER_GROUP_RESPONSE;

    public long currentGroupId;

}
