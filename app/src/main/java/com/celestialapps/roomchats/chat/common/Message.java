package com.celestialapps.roomchats.chat.common;

import java.io.Serializable;

public class Message implements Serializable{
   private final MessageType messageType;
   private String userName;
   private String roomName;
   private String text;

   public Message(MessageType messageType) {
       this.messageType = messageType;
   }

    public Message(String text, MessageType messageType) {
       this.text = text;
       this.messageType = messageType;
    }

   public Message(MessageType messageType, String userName) {
       this.messageType = messageType;
       this.userName = userName;
   }


    public Message(MessageType messageType, String userName, String text) {
        this.messageType = messageType;
        this.userName = userName;
        this.text = text;
    }

    public Message(MessageType messageType, String userName, String roomName, String text) {
        this.messageType = messageType;
        this.userName = userName;
        this.roomName = roomName;
        this.text = text;
    }


   public MessageType getMessageType() {
       return messageType;
   }

   public String getText() {
       return text;
   }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return userName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
