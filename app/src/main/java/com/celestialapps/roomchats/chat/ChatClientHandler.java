package com.celestialapps.roomchats.chat;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.celestialapps.roomchats.activity.adapters.RoomAdapter;
import com.celestialapps.roomchats.chat.common.Message;
import com.celestialapps.roomchats.chat.common.MessageType;
import com.celestialapps.roomchats.model.User;

import java.util.List;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Sergey on 13.04.2017.
 */
public class ChatClientHandler extends SimpleChannelInboundHandler<Message> {
    private final String TAG = "ChatClientHandler";

    private String userName;
    private String userPassword;
    private String roomName;
    private String roomPassword;

    private RecyclerView roomMessageList;

    ChatClientHandler(User user, RecyclerView roomMessageList) {
        this.userName = user.getLogin();
        this.roomName = user.getCurrentRoom().getName();
        this.userPassword = user.getPassword();
        this.roomPassword = user.getCurrentRoom().getPassword();
        this.roomMessageList = roomMessageList;
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        Log.d(TAG, "handlerAdded()");
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, final Message message) throws Exception {
        Log.d(TAG, "messageReceived()");
        Log.d(TAG, message.getMessageType().toString());

        final Channel channel = ctx.channel();

        switch (message.getMessageType()) {
            case NAME_REQUEST:
                channel.writeAndFlush(new Message(MessageType.USER_NAME, userName, userPassword));
                break;
            case USER_GROUP_REQUEST:
                channel.writeAndFlush(new Message(MessageType.USER_GROUP_RESPONSE, userName, roomName, roomPassword));
                break;
            case USER_ADDED:
                break;
            case TEXT:
                Observable.just(message.getText())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(s -> {
                            RoomAdapter roomAdapter = (RoomAdapter) roomMessageList.getAdapter();
                            roomAdapter.getMessages().add(message.getText());
                            roomAdapter.notifyDataSetChanged();
                            roomMessageList.scrollToPosition(roomAdapter.getItemCount() - 1);
                        }, throwable -> {
                            Log.d(TAG, throwable.getMessage(), throwable);
                        });
                break;
        }
    }


    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        Log.d(TAG, cause.getMessage(), cause);
        ctx.close();
    }

}
