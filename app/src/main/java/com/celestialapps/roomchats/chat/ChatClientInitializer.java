package com.celestialapps.roomchats.chat;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.celestialapps.roomchats.activity.adapters.RoomAdapter;
import com.celestialapps.roomchats.chat.common.Message;
import com.celestialapps.roomchats.model.User;

import java.util.List;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * Created by Sergey on 13.04.2017.
 */
public class ChatClientInitializer extends ChannelInitializer<SocketChannel> {
    private final String TAG = "ChatClientInitializer";

    private User user;
    private RecyclerView roomMessageList;


    public ChatClientInitializer(User user, RecyclerView roomMessageList) {
        this.user = user;
        this.roomMessageList = roomMessageList;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        Log.d(TAG, "initChannel()");
        ChannelPipeline pipeline = socketChannel.pipeline();

        pipeline.addLast("decoder", new ObjectDecoder(ClassResolvers.weakCachingResolver(Message.class.getClassLoader())));
        pipeline.addLast("encoder", new ObjectEncoder());

        pipeline.addLast("handler", new ChatClientHandler(user, roomMessageList));
    }
}
